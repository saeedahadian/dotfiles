module.exports = {
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module',
    allowImportExportEverywhere: false,
    ecmaFeatures: {
      globalReturn: false,
    },
  },
  extends: [
    'airbnb-base',
    'plugin:promise/recommended',
    'prettier',
    'plugin:vue/vue3-recommended',
    'prettier',
    'prettier/vue',
  ],
  plugins: ['promise', 'prettier'],
  rules: {
    'prettier/prettier': 'error',
    indent: ['error', 2, { SwitchCase: 1 }],
    semi: 'error',
    strict: 'off',
    'import/extensions': 'off',
  },
  env: {
    browser: true,
    node: true,
    commonjs: true,
    jquery: true,
    mongo: true,
    es6: true,
    serviceworker: true,
  },
};
