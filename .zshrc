# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

#=======================================================================
#   ZSH Initial Installation Configs
#-----------------------------------------------------------------------
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
#bindkey -v     # Disable vim key-binding
# End of lines configured by zsh-newuser-install

# The following lines were added by compinstall
zstyle :compinstall filename '/home/saeed/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall


#=======================================================================
#   User Specific Configurations
#-----------------------------------------------------------------------
# Aliases
source $HOME/.aliases

# Add AppImages dir to the path.
path+=$HOME/Downloads/AppImages
path+=$HOME/.deno/bin
path+=$HOME/.cargo/env

# Add "DOOM Emacs" to your path.
path+=$HOME/.emacs.d/bin

# Add completion scripts
fpath+=~/.zsh/completion

# Add Tex Live 2019 to your path.
manpath+=/usr/local/texlive/2019/texmf-dist/doc/man
infopath+=/usr/local/texlive/2019/texmf-dist/doc/info
path+=/usr/local/texlive/2019/bin/x86_64-linux

# Add docker credential helper to path.
path+=$HOME/.docker

# Imitate bash's behavior with Ctrl+U
bindkey \^U backward-kill-line

#=======================================================================
#   Powerlevel10k Theme
#-----------------------------------------------------------------------
# Set the theme on powerlevel10k.
source ~/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


#=======================================================================
#   Node Version Manager (NVM)
#-----------------------------------------------------------------------
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# ZSH Syntax Highlighting like Fish
source /home/saeed/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
